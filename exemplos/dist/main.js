"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fatorial_1 = require("./fatorial");
var yargs = require("yargs");
console.log('=== n-fatorial ===');
var argv = yargs.demandOption('num').argv;
var num = argv.num;
console.log("O fatorial de " + num + " \u00E9 igual a " + fatorial_1.fatorial(num));
//console.log(module.paths)
//# sourceMappingURL=main.js.map
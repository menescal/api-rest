import * as restify from 'restify'
import { environment } from './../cammon/environment';
import { Router } from '../cammon/router'

export class Server {

    application: restify.Server

    initRoutes(routers): Promise<any>{
        return new Promise((resolve, reject)=>{
            try {
                this.application = restify.createServer({
                    name: 'meat-api',
                    version: '1.0.0'
                })
                
                this.application.use(restify.plugins.queryParser())

                //routes
                for(let router of routers){
                    router.applyRoutes(this.application)
                }

                this.application.listen(environment.server.port, ()=>{
                    resolve(this.application)
                })

            } catch (error) {
                reject(error)
            }
        })
    }

    //A promise retorna a própria classe Server
    bootstrap(routers: Router[] = []): Promise<Server>{
        return this.initRoutes(routers).then(()=> this)
    }
}